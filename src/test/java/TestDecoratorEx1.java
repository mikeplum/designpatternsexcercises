import decorator.ex1.ChrismasTree;
import decorator.ex1.ChristmasTreeImpl;
import decorator.ex1.decorator.TreeDecorator;
import decorator.ex1.decorator.addons.BubbleLights;
import decorator.ex1.decorator.addons.TreeTopper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestDecoratorEx1 {

    @Test
    public void Decorator_whenDecoratorsInjectedAtRuntime_thenConfigSuccess() {

        ChrismasTree tree1 = new BubbleLights(new ChristmasTreeImpl());
        assertEquals(tree1.decorate(),"Christmas tree with Bubble Lights" );

        ChrismasTree tree2 = new TreeTopper(tree1);
        assertEquals(tree2.decorate(), "Christmas tree with Bubble Lights with TreeTopper");

        ChrismasTree tree3 = new BubbleLights(new TreeTopper(new ChristmasTreeImpl()));
        assertEquals(tree2.decorate(), "Christmas tree with Bubble Lights with TreeTopper");
    }

}
