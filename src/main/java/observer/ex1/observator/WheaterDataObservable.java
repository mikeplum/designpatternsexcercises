package observer.ex1.observator;

public interface WheaterDataObservable {

    void addSubscriber(WheaterDataObeserver obeserver);
    void removeSubscriber(WheaterDataObeserver obeserver);
    void notifySubscribers();

}
