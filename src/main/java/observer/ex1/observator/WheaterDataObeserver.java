package observer.ex1.observator;

import observer.ex1.wheaterdata.ReadingsData;

public interface WheaterDataObeserver {

    void update(ReadingsData readingsData);

}
