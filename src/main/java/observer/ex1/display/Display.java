package observer.ex1.display;

public interface Display {

    void display();

}
