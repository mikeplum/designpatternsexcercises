package observer.ex1.display;

import observer.ex1.observator.WheaterDataObeserver;
import observer.ex1.observator.WheaterDataObservable;
import observer.ex1.wheaterdata.ReadingsData;

public class CurrentConditions implements Display, WheaterDataObeserver {

    private float temp;
    private float humidity;
    private WheaterDataObservable wheaterDataObservable;

    public CurrentConditions(WheaterDataObservable wheaterDataObservable) {
        this.wheaterDataObservable = wheaterDataObservable;
        wheaterDataObservable.addSubscriber(this);
    }

    @Override
    public void display() {
        System.out.println("Current condition changes | Temp: " + temp + " | Humidity: " + humidity);
    }

    @Override
    public void update(ReadingsData readingsData) {
        this.temp = readingsData.getTemp();
        this.humidity = readingsData.getHumidity();
        display();
    }

    @Override
    protected void finalize() {
        wheaterDataObservable.removeSubscriber(this);
    }
}
