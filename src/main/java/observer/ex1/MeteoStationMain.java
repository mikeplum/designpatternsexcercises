package observer.ex1;

import observer.ex1.display.CurrentConditions;
import observer.ex1.wheaterdata.ReadingsData;
import observer.ex1.wheaterdata.WheaterData;

public class MeteoStationMain {
    public static void main(String[] args) {

        WheaterData wheaterData = new WheaterData();
        CurrentConditions currentConditions = new CurrentConditions(wheaterData);

        wheaterData.setState(new ReadingsData(2.2f, 3.2f, 4.2f));
        wheaterData.setState(new ReadingsData(2.3f, 3.3f, 4.3f));

    }
}
