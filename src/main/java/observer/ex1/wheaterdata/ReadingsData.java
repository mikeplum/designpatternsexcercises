package observer.ex1.wheaterdata;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReadingsData {

    float temp;
    float humidity;
    float pressure;

}
