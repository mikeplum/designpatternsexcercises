package observer.ex1.wheaterdata;

import observer.ex1.observator.WheaterDataObeserver;
import observer.ex1.observator.WheaterDataObservable;

import java.util.ArrayList;
import java.util.List;

public class WheaterData implements WheaterDataObservable {

    private ReadingsData wheaterDataState;

    private List<WheaterDataObeserver> subscribers = new ArrayList<>();

    public ReadingsData getState() {
        return wheaterDataState;
    }

    public void setState(ReadingsData wheaterDataState) {
        this.wheaterDataState = wheaterDataState;
        readingChange();
    }

    @Override
    public void addSubscriber(WheaterDataObeserver obeserver) {
        subscribers.add(obeserver);
    }

    @Override
    public void removeSubscriber(WheaterDataObeserver obeserver) {
        int i = subscribers.indexOf(obeserver);
        if ( i >= 0 ) {
         subscribers.remove(i);
        }
        //Check if can be replaced with: subscribers.remove(obeserver);
    }

    @Override
    public void notifySubscribers() {
        subscribers.stream().forEach( s -> s.update(getState()));
    }

    public void readingChange() {
        notifySubscribers();
    }

}
