package decorator.ex1;

public interface ChrismasTree {

    String decorate();

}
