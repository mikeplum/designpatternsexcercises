package decorator.ex1.decorator;

import decorator.ex1.ChrismasTree;

public class TreeDecorator implements ChrismasTree {

    private ChrismasTree tree;

    public TreeDecorator(ChrismasTree tree) {
        this.tree = tree;
    }

    @Override
    public String decorate() {
        return tree.decorate();
    }
}
