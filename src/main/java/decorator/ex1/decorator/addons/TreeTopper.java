package decorator.ex1.decorator.addons;

import decorator.ex1.ChrismasTree;
import decorator.ex1.decorator.TreeDecorator;

public class TreeTopper extends TreeDecorator {

    public TreeTopper(ChrismasTree tree) {
        super(tree);
    }

    public String decorate() {
        return super.decorate() + decorateWithTreeTopper();
    }

    private String decorateWithTreeTopper(){
        return " with TreeTopper";
    }
}
