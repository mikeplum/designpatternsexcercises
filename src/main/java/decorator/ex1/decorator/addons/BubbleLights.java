package decorator.ex1.decorator.addons;

import decorator.ex1.ChrismasTree;
import decorator.ex1.decorator.TreeDecorator;

public class BubbleLights extends TreeDecorator {

    public BubbleLights(ChrismasTree tree) {
        super(tree);
    }

    public String decorate() {
        return super.decorate() + decorateWithBubbleLights();
    }

    private String decorateWithBubbleLights() {
        return " with Bubble Lights";
    }

}
