package decorator.ex1;

public class ChristmasTreeImpl implements ChrismasTree {

    @Override
    public String decorate() {
        return "Christmas tree";
    }
}
